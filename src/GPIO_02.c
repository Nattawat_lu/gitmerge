#include "GPIO_02.h"
#include <inttypes.h>
void HAL_GPIO_Init(GPIO_TypeDef *GPIOx, GPIO_InitTypeDef *GPIO_Init)
{
  uint32_t position;
  uint32_t ioposition = 0x00U;
  uint32_t iocurrent = 0x00U;
  uint32_t temp = 0x00U;
 

  assert_param(IS_GPIO_ALL_INSTANCE(GPIOx));
  assert_param(IS_GPIO_PIN(GPIO_Init->Pin));
  assert_param(IS_GPIO_MODE(GPIO_Init->Mode));
  assert_param(IS_GPIO_PULL(GPIO_Init->Pull));


  //for(position = 0U; position < 16U; position++)
  //{

    /*ioposition = 0x01U << position;
    iocurrent = (uint32_t)(GPIO_Init->Pin) & ioposition;

    if(iocurrent == ioposition)
    {*/


      if((GPIO_Init->Mode == 0x00000002U) || (GPIO_Init->Mode == 0x00000012U))
      {

        assert_param(IS_GPIO_AF(GPIO_Init->Alternate)); 	

        temp = GPIOx->AFR[position >> 3U];
        temp &= ~(0xFU << ((uint32_t)(position & 0x07U) * 4U)) ;
        temp |= ((uint32_t)(GPIO_Init->Alternate) << (((uint32_t)position & 0x07U) * 4U));
        GPIOx->AFR[position >> 3U] = temp;
      }


      temp = GPIOx->MODER;
      temp &= ~((0x3UL << (0U)) << (position * 2U));
      temp |= ((GPIO_Init->Mode & 0x00000003U) << (position * 2U));
      GPIOx->MODER = temp;


      if((GPIO_Init->Mode == 0x00000001U) || (GPIO_Init->Mode == 0x00000002U) ||
         (GPIO_Init->Mode == 0x00000011U) || (GPIO_Init->Mode == 0x00000012U))
      {

        assert_param(IS_GPIO_SPEED(GPIO_Init->Speed));

        temp = GPIOx->OSPEEDR;
        temp &= ~((0x3UL << (0U)) << (position * 2U));
        temp |= (GPIO_Init->Speed << (position * 2U));
        GPIOx->OSPEEDR = temp;


        temp = GPIOx->OTYPER;
        temp &= ~((0x1UL << (0U)) << position) ;
        temp |= (((GPIO_Init->Mode & 0x00000010U) >> 4U) << position);
        GPIOx->OTYPER = temp;
      }


      temp = GPIOx->PUPDR;
      temp &= ~((0x3UL << (0U)) << (position * 2U));
      temp |= ((GPIO_Init->Pull) << (position * 2U));
      GPIOx->PUPDR = temp;



      if((GPIO_Init->Mode & 0x10000000U) == 0x10000000U)
      {

        do { volatile uint32_t tmpreg = 0x00U; ((((RCC_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x3800UL))->APB2ENR) |= ((0x1UL << (14U)))); tmpreg = ((((RCC_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x3800UL))->APB2ENR) & ((0x1UL << (14U)))); (void)tmpreg; } while(0U);

        temp = ((SYSCFG_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3800UL))->EXTICR[position >> 2U];
        temp &= ~(0x0FU << (4U * (position & 0x03U)));
        temp |= ((uint32_t)((uint8_t)(((GPIOx) == (((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x0000UL))))? 0U : ((GPIOx) == (((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x0400UL))))? 1U : ((GPIOx) == (((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x0800UL))))? 2U : ((GPIOx) == (((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x0C00UL))))? 3U : ((GPIOx) == (((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x1000UL))))? 4U : 7U)) << (4U * (position & 0x03U)));
        ((SYSCFG_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3800UL))->EXTICR[position >> 2U] = temp;


        temp = ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->IMR;
        temp &= ~((uint32_t)iocurrent);
        if((GPIO_Init->Mode & 0x00010000U) == 0x00010000U)
        {
          temp |= iocurrent;
        }
        ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->IMR = temp;

        temp = ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->EMR;
        temp &= ~((uint32_t)iocurrent);
        if((GPIO_Init->Mode & 0x00020000U) == 0x00020000U)
        {
          temp |= iocurrent;
        }
        ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->EMR = temp;


        temp = ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->RTSR;
        temp &= ~((uint32_t)iocurrent);
        if((GPIO_Init->Mode & 0x00100000U) == 0x00100000U)
        {
          temp |= iocurrent;
        }
        ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->RTSR = temp;

        temp = ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->FTSR;
        temp &= ~((uint32_t)iocurrent);
        if((GPIO_Init->Mode & 0x00200000U) == 0x00200000U)
        {
          temp |= iocurrent;
        }
        ((EXTI_TypeDef *) ((0x40000000UL + 0x00010000UL) + 0x3C00UL))->FTSR = temp;
      }
    //}
  //}
}

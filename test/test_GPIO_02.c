#include "unity.h"
#include "GPIO_02.h"

void setUp(void)
{
}

void tearDown(void)
{
}
//#define   GPIOD ((GPIO_TypeDef *) ((0x40000000UL + 0x00020000UL) + 0x0C00UL))

void test_GPIO_02(void)
{
	//GPIO_TypeDef *GPIOx;
    GPIO_InitTypeDef *GPIO_Init;

  GPIO_Init->Pin = ((uint16_t)0x1000);
  GPIO_Init->Mode = 0x00000002U;
  GPIO_Init->Pull = 0x00000000U;
  GPIO_Init->Speed = 0x00000000U;

  HAL_GPIO_Init(GPIOD, GPIO_Init);
}

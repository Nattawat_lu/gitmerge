#include "unity.h"
#include "hal_usart.h"

HAL_StatusTypeDef Usart_status;


void setUp(void)
{
}
void tearDown(void)
{
}

void test_hal_usart_NeedToImplement(void)
{
	USART_HandleTypeDef *husart,Phusart;
	uint8_t VpRxData;
	uint8_t *pRxData;
	uint16_t Size;
	int a[104];



	/****************************  Set value structure by array   *********************************/
    
    /*
    int a[104];
    	for (int i = 0; i < 104; i++)
	{
		a[i] = 2;
	}*/
 	/*husart = a;
    husart->State = HAL_USART_STATE_READY;*/



    /****************************  Case 1   *********************************/
    Phusart.State = HAL_USART_STATE_READY;
	husart = &Phusart;
	Size = 0;
    Usart_status = HAL_USART_Receive_IT(husart,pRxData,Size);

    /****************************  Case 2   *********************************/
    Phusart.State = HAL_USART_STATE_RESET;
    Usart_status = HAL_USART_Receive_IT(husart,pRxData,Size);

    /****************************  Case 3   *********************************/
    Phusart.State = HAL_USART_STATE_READY;
    husart = &Phusart;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    Size = 2;
    Usart_status = HAL_USART_Receive_IT(husart,pRxData,Size);
}
